Nova.booting((Vue, router, store) => {
    Vue.component('index-custom-date-time', require('./components/IndexField'))
    Vue.component('detail-custom-date-time', require('./components/DetailField'))
    Vue.component('form-custom-date-time', require('./components/FormField'))
    Vue.component('form-custom-date-time-picker', require('./components/DateTimePicker'))
})
