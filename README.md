# Laravel Nova Custom DateTime Picker Field
The API is exactly the same as Default Nova DateTime field with additional features:
- Can set Custom Default Hour, Minute and Seconds in DateTime Picker.
#Usage
```php
<?php

use Adorebeauty\CustomDateTime\CustomDateTime;

CustomDateTime::make('End Date', 'end_date')
                ->pickerDefaultHour(23)  //Add default hour
                ->pickerDefaultMinute(59) //Add default minute
                ->pickerDefaultSeconds(59), //Add default seconds 

```
