<?php

namespace Adorebeauty\CustomDateTime;

use Laravel\Nova\Fields\DateTime;

class CustomDateTime extends DateTime
{
    /**
     * The field's component.
     *
     * @var string
     */
    public $component = 'custom-date-time';

    /**
     * Set default hour
     * @param  int  $int
     * @return CustomDateTime
     */
    public function pickerDefaultHour(int $int)
    {
        return $this->withMeta([__FUNCTION__ => $int]);
    }

    /**
     * Set default minute
     * @param  int  $int
     * @return CustomDateTime
     */
    public function pickerDefaultMinute(int $int)
    {
        return $this->withMeta([__FUNCTION__ => $int]);
    }

    /**
     * Set default seconds
     * @param  int  $int
     * @return CustomDateTime
     */
    public function pickerDefaultSeconds(int $int)
    {
        return $this->withMeta([__FUNCTION__ => $int]);
    }
}
